import org.jetbrains.compose.ExperimentalComposeLibrary

plugins {
    kotlin("multiplatform")
    id("com.android.library")
    alias(libraries.plugins.jetbrainsCompose)
    id("io.kotest.multiplatform") version "5.3.0"
}

kotlin {
    android()

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared"
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                api(compose.ui)
                api(compose.foundation)
                @OptIn(ExperimentalComposeLibrary::class)
                api(compose.material3)
                api(libraries.koinCore)
                api(libraries.coroutinesCore)
                implementation("io.arrow-kt:arrow-core:1.1.3")
                implementation("io.arrow-kt:arrow-optics:1.1.3")
                implementation("io.arrow-kt:arrow-fx-coroutines:1.1.3")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(libraries.koinTest)
                implementation(libraries.coroutinesTest)
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
            dependencies {
                api(libraries.coroutinesAndroid)
                api(compose.uiTooling)
                api(compose.preview)
                implementation(libraries.ktorClientAndroid)
            }
        }
        val androidTest by getting
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
            dependencies {
                implementation(libraries.ktorClientIOs)
            }
        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            dependsOn(commonTest)
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
        }
    }
}

android {
    compileSdk = 33
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
    defaultConfig {
        minSdk = 21
        targetSdk = 33
    }
    namespace = "fr.hadaly.kmp"
}
