package fr.hadaly.kmp

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform}!"
    }
}
