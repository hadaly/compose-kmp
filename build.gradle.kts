import io.gitlab.arturbosch.detekt.Detekt

buildscript {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
    dependencies {
        classpath(libraries.kotlinSupport)
        classpath(libraries.agp)
    }
}

plugins {
    alias(libraries.plugins.detekt)
}

val yml: Configuration by configurations.creating

dependencies {
    yml("fr.hadaly.detekt-config:detekt:1.21.0-1@yml")
    yml("fr.hadaly.detekt-config:compose:1.21.0-1@yml")
}

dependencies {
    detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.21.0")
    detektPlugins("com.twitter.compose.rules:detekt:0.0.22")
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

detekt {
    buildUponDefaultConfig = true
    allRules = false
    config = files(yml.allArtifacts.files)
    baseline = file("$projectDir/config/baseline.xml")
}

tasks.register<Detekt>("detektAll") {
    description = "Runs over whole code base without the starting overhead for each module."
    parallel = true
    setSource(files(projectDir))
    include("**/*.kt")
    include("**/*.kts")
    exclude("**/resources/**")
    exclude("**/build/**")
    reports {
        xml {
            enabled = true
        }
        html {
            enabled = true
        }
        txt {
            enabled = true
        }
    }
}

tasks.register<Detekt>("detektFormat") {
    description = "Runs over whole code base without the starting overhead for each module."
    parallel = true
    autoCorrect = true
    setSource(files(projectDir))
    include("**/*.kt")
    include("**/*.kts")
    exclude("**/resources/**")
    exclude("**/build/**")
    reports {
        xml {
            enabled = true
        }
        html {
            enabled = true
        }
        txt {
            enabled = true
        }
    }
}
