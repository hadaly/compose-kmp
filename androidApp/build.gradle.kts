plugins {
    id("com.android.application")
    kotlin("android")
    alias(libraries.plugins.jetbrainsCompose)
    alias(libraries.plugins.detekt)
}

android {
    compileSdk = 33
    defaultConfig {
        applicationId = "fr.hadaly.kmp.android"
        namespace = "fr.hadaly.kmp.android"
        minSdk = 21
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }

    kotlinOptions {
        jvmTarget = "11"
    }
}

dependencies {
    implementation(project(":shared"))
    implementation(libraries.koinAndroid)
    implementation(libraries.koinWorkManager)
    implementation(libraries.koinCompose)
    implementation(libraries.activityCompose)
    implementation(libraries.appCompat)
    implementation(libraries.constraintLayoutCompose)
}
