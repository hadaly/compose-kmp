package fr.hadaly.kmp.android

import android.os.Bundle
import android.widget.TextView
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.WindowInsets
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.core.view.WindowCompat
import fr.hadaly.kmp.Greeting

fun greet(): String {
    return Greeting().greeting()
}

@OptIn(ExperimentalMaterial3Api::class)
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)
        setContent {
            KmpTheme {
                Scaffold(
                    topBar = {
                        SmallTopAppBar(title = { Text("App Name") })
                    }
                ) {
                    Text("Hello World !")
                }
            }
        }
    }
}
