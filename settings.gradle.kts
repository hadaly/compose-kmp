pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
    }
}

dependencyResolutionManagement {
    repositories {
        google()
        mavenCentral()
        maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
        maven {
            url = uri("https://gitlab.com/api/v4/groups/hadaly/-/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = System.getenv("PUBLISH_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
            content {
                includeGroupByRegex("fr.hadaly.*")
            }
        }
    }
    versionCatalogs {
        create("libraries") {
            from("fr.hadaly.catalog:kmm:1.1.0")
            version("kotlin", "1.7.20")
            version("jetbrainsCompose", "1.3.0-beta02")
        }
    }
}

rootProject.name = "kmp"
include(":androidApp")
include(":shared")
